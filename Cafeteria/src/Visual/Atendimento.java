
package Visual;

import controle.ClienteControle;
import Modelo.Cliente;
import Modelo.Servico;
import Modelo.Usuario;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Biscoito
 */
public class Atendimento extends javax.swing.JFrame {

    /**
     * Creates new form Filmes
     */
    public Atendimento() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        Tabelita = new javax.swing.JScrollPane();
        tbl_Atendimento = new javax.swing.JTable();
        btn_Salvar = new javax.swing.JButton();
        bnt_pagar = new javax.swing.JButton();
        bnt_Cancelar = new javax.swing.JButton();
        clicli = new javax.swing.JTextField();
        Opcao = new javax.swing.JComboBox();
        jButton1 = new javax.swing.JButton();
        opção = new javax.swing.JLabel();
        Sorvete = new javax.swing.JLabel();
        preco = new javax.swing.JTextField();
        cliente = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel2.setFont(new java.awt.Font("David", 1, 48)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Atendimento");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(580, 40, 360, -1));

        Tabelita.setBackground(new java.awt.Color(0, 0, 51));
        Tabelita.setFont(new java.awt.Font("Tw Cen MT", 0, 12)); // NOI18N

        tbl_Atendimento.setBackground(new java.awt.Color(255, 153, 153));
        tbl_Atendimento.setFont(new java.awt.Font("Tw Cen MT", 0, 11)); // NOI18N
        tbl_Atendimento.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Cliente", "Opção", "Preço"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_Atendimento.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        tbl_Atendimento.setGridColor(new java.awt.Color(153, 153, 153));
        tbl_Atendimento.setSelectionBackground(new java.awt.Color(255, 153, 153));
        tbl_Atendimento.setSelectionForeground(new java.awt.Color(0, 0, 0));
        tbl_Atendimento.getTableHeader().setReorderingAllowed(false);
        Tabelita.setViewportView(tbl_Atendimento);

        getContentPane().add(Tabelita, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 170, 520, 380));

        btn_Salvar.setBackground(new java.awt.Color(102, 102, 255));
        btn_Salvar.setFont(new java.awt.Font("Tw Cen MT", 0, 15)); // NOI18N
        btn_Salvar.setForeground(new java.awt.Color(0, 0, 51));
        btn_Salvar.setText("Salvar");
        btn_Salvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_SalvarActionPerformed(evt);
            }
        });
        getContentPane().add(btn_Salvar, new org.netbeans.lib.awtextra.AbsoluteConstraints(733, 600, 120, -1));

        bnt_pagar.setBackground(new java.awt.Color(0, 204, 51));
        bnt_pagar.setFont(new java.awt.Font("Tw Cen MT", 0, 15)); // NOI18N
        bnt_pagar.setForeground(new java.awt.Color(0, 0, 51));
        bnt_pagar.setText("Pagar");
        bnt_pagar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bnt_pagarActionPerformed(evt);
            }
        });
        getContentPane().add(bnt_pagar, new org.netbeans.lib.awtextra.AbsoluteConstraints(890, 600, 120, -1));

        bnt_Cancelar.setBackground(new java.awt.Color(255, 0, 51));
        bnt_Cancelar.setFont(new java.awt.Font("Tw Cen MT", 0, 15)); // NOI18N
        bnt_Cancelar.setForeground(new java.awt.Color(0, 0, 51));
        bnt_Cancelar.setText("Cancelar");
        bnt_Cancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bnt_CancelarActionPerformed(evt);
            }
        });
        getContentPane().add(bnt_Cancelar, new org.netbeans.lib.awtextra.AbsoluteConstraints(1040, 600, 120, 30));

        clicli.setFont(new java.awt.Font("Tw Cen MT", 0, 12)); // NOI18N
        getContentPane().add(clicli, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 230, 250, 30));

        Opcao.setFont(new java.awt.Font("Tw Cen MT", 0, 11)); // NOI18N
        Opcao.setForeground(new java.awt.Color(255, 255, 255));
        Opcao.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "", "Capuccino", "Sorvete", "Açaí e Sorvete" }));
        getContentPane().add(Opcao, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 400, 250, 30));

        jButton1.setText("Cadastrar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 280, 90, 30));

        opção.setFont(new java.awt.Font("David", 1, 18)); // NOI18N
        opção.setForeground(new java.awt.Color(255, 255, 255));
        opção.setText("Opções do Menu :");
        getContentPane().add(opção, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 360, -1, -1));

        Sorvete.setFont(new java.awt.Font("David", 1, 18)); // NOI18N
        Sorvete.setForeground(new java.awt.Color(255, 255, 255));
        Sorvete.setText("Preço R$:");
        getContentPane().add(Sorvete, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 450, -1, -1));
        getContentPane().add(preco, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 490, 250, 30));

        cliente.setFont(new java.awt.Font("David", 1, 18)); // NOI18N
        cliente.setForeground(new java.awt.Color(255, 255, 255));
        cliente.setText("Nome do Cliente :");
        getContentPane().add(cliente, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 200, 140, -1));

        jLabel3.setIcon(new javax.swing.ImageIcon("C:\\Users\\Biscoito\\Downloads\\img\\Agenda-PainelFundo.png")); // NOI18N
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 30, 1220, 650));

        jLabel1.setIcon(new javax.swing.ImageIcon("C:\\Users\\Biscoito\\Downloads\\img\\3.jpg")); // NOI18N
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, -30, 1480, 770));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_SalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_SalvarActionPerformed
        DefaultTableModel dtmTable = (DefaultTableModel) tbl_Atendimento.getModel();
        //Criçao de uma array para armazenar as info. dos campos de texto e colocar na tabela
        Object[] dados = { preco.getText(), Opcao.getSelectedItem()};
        dtmTable.addRow(dados);
        //limpar os campos apos salvar os dados na tabela
        Opcao.setSelectedItem(null);
        preco.setText("");
        clicli.setText("");
        Cliente cli = new Cliente();
        Usuario usu = new Usuario();
        Servico ser = new Servico();
        ser.setOpcao(Opcao.getSelectedItem());
        ser.setPreco(preco.getText());
        
        
    }//GEN-LAST:event_btn_SalvarActionPerformed

    private void bnt_pagarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bnt_pagarActionPerformed
        //O campo sera selecionado e excluido
        if(tbl_Atendimento.getSelectedRow() != -1){
            DefaultTableModel dmtTable = (DefaultTableModel) tbl_Atendimento.getModel();
            dmtTable.removeRow(tbl_Atendimento.getSelectedRow());

        }else{
            JOptionPane.showMessageDialog(null, "Você não selecionou nada");

        }

    }//GEN-LAST:event_bnt_pagarActionPerformed

    private void bnt_CancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bnt_CancelarActionPerformed
        if(tbl_Atendimento.getSelectedRow() != -1){
            DefaultTableModel dmtTable = (DefaultTableModel) tbl_Atendimento.getModel();
            dmtTable.removeRow(tbl_Atendimento.getSelectedRow());
        }else{
            JOptionPane.showMessageDialog(null, "Você não selecionou nada");

        }
    }//GEN-LAST:event_bnt_CancelarActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        Cliente cli = new Cliente();
        cli.setNome(clicli.getText());
        if(clicli.getText().isEmpty()){
            JOptionPane.showMessageDialog(null, "O campo não pode retornar vazio");
        }else{
            ClienteControle clie = new ClienteControle();
            clie.inserir(cli);
            JOptionPane.showMessageDialog(null, "Cliente cadastrado com sucesso");
            
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Atendimento.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Atendimento.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Atendimento.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Atendimento.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new Atendimento().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox Opcao;
    private javax.swing.JLabel Sorvete;
    private javax.swing.JScrollPane Tabelita;
    private javax.swing.JButton bnt_Cancelar;
    private javax.swing.JButton bnt_pagar;
    private javax.swing.JButton btn_Salvar;
    private javax.swing.JTextField clicli;
    private javax.swing.JLabel cliente;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel opção;
    private javax.swing.JTextField preco;
    private javax.swing.JTable tbl_Atendimento;
    // End of variables declaration//GEN-END:variables
}
