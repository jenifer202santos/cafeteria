
package Modelo;


public class Servico {
    private int idServico;
    private Object opcao;
    private String preco;

    public int getIdServico() {
        return idServico;
    }

    public void setIdServico(int idServico) {
        this.idServico = idServico;
    }

    public Object getOpcao() {
        return opcao;
    }

    public void setOpcao(Object opcao) {
        this.opcao = opcao;
    }

    public String getPreco() {
        return preco;
    }

    public void setPreco(String preco) {
        this.preco = preco;
    }

    
    
}