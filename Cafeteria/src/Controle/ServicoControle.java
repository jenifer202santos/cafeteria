
package Controle;

import Modelo.Cliente;
import Modelo.Servico;
import Modelo.Usuario;
import java.sql.PreparedStatement;
import java.sql.SQLException;


public class ServicoControle {
    private final ServicoControle view;
    public ServicoControle(ServicoControle view){
        this.view = view;
    }
    public boolean inserir(ServicoControle atendimento, Usuario user, Cliente cli, Servico ser){
        boolean resultado = false;
        try{
            Conexao con = new Conexao();
            String sql = "INSERT INTO tabela(idUser, idCliente, idServico) VALUES((SELECT idUser FROM usuario WHERE user = ?),(SELECT idCliente FROM cliente WHERE nome = ?),(SELECT idServico WHERE opcao = ? ));";
            PreparedStatement ps = con.getConexao().preparedStatement(sql);
            ps.setInt(1, user.getId());
            ps.setInt(2, cli.getIdCliente());
            ps.setInt(3, ser.getIdServico());
            if(!ps.execute()){
                resultado = true;
            }
            
        }catch(SQLException e){
            System.out.println("Erro no banco:" + e.getMessage());
          
        }
        return resultado;
    }
   
}
