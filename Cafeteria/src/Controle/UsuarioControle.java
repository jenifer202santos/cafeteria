package controle;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import Modelo.Usuario;
import java.util.ArrayList;
public class UsuarioControle {
    public boolean selecionarUm(Usuario user){
        boolean resultado = false;
        try{
            Conexao con = new Conexao();
            String sql = "SELECT * FROM usuario WHERE user = ? AND senha = ?;";
            PreparedStatement ps = con.getConexao().prepareStatement(sql);
            ps.setString(1, user.getUser());
            ps.setString(2, user.getSenha());
            if(ps.execute()){
                ResultSet rs = ps.executeQuery();
                if(rs != null){
                    while(rs.next()){
                       resultado = true;
                    }
                }
            }
        }catch(Exception e){
            System.out.println("Erro no banco   " + e.getMessage());
        }
        return resultado;
    }
	public ArrayList<Usuario> selecionarTodos() {
		ArrayList<Usuario> lista = null;
		try {
			Conexao con = new Conexao();
			String sql = "SELECT * FROM usuario;";
			PreparedStatement ps = con.getConexao().prepareStatement(sql);
			if(ps.execute()){
				ResultSet rs = ps.executeQuery();
				if(rs != null) {
					lista = new ArrayList<Usuario>();
					while(rs.next()) {
						Usuario user = new Usuario();
						user.setId(rs.getInt("id"));
						user.setEmail(rs.getString("email"));
						user.setUser(rs.getString("user"));
						user.setSenha(rs.getString("senha"));
                                                user.setCpf(rs.getString("cpf"));
						lista.add(user);
					}
				}
			}
		}catch(SQLException e) {
			System.out.println("Erro do banco: " + e.getMessage());
		}
		return lista;
	}
	public boolean inserir(Usuario user) {
		boolean resultado = false;
		try {
			Conexao con = new Conexao();
			String sql = "INSERT INTO usuario(user,email,senha,cpf) VALUES(?,?,?,?);";
			PreparedStatement ps = con.getConexao().prepareStatement(sql);
			ps.setString(1, user.getUser());
			ps.setString(2, user.getEmail());
			ps.setString(3, user.getSenha());
                        ps.setString(4, user.getCpf());
			if(!ps.execute()) {
				resultado = true;
			}
		}catch(SQLException e) {
			System.out.println("Erro no banco: " + e.getMessage());
		}
		return resultado;
	}
	public boolean atualizar() {
		boolean resultado = false;
		
		return resultado;
	}
	public boolean deletar() {
		boolean resultado = false;
		
		return resultado;
	}
	
	
}


