package controle;
import Modelo.Cliente;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
public class ClienteControle {
    public boolean selecionarUm(Cliente cli){
        boolean resultado = false;
        try{
            Conexao bon = new Conexao();
            String sql = "SELECT * FROM cliente WHERE nome = ?;";
            PreparedStatement ps = bon.getConexao().prepareStatement(sql);
            ps.setString(1, cli.getNome());
            if(ps.execute()){
                ResultSet rs = ps.executeQuery();
                if(rs != null){
                    while(rs.next()){
                       resultado = true;
                    }
                }
            }
        }catch(Exception e){
            System.out.println("Erro no banco   " + e.getMessage());
        }
        return resultado;
    }
	
	public boolean inserir(Cliente cli) {
		boolean resultado = false;
		try {
			Conexao con = new Conexao();
			String sql = "INSERT INTO cliente(nome) VALUES(?);";
			PreparedStatement ps = con.getConexao().prepareStatement(sql);
			ps.setString(1, cli.getNome());
			if(!ps.execute()) {
				resultado = true;
			}
		}catch(SQLException e) {
			System.out.println("Erro no banco: " + e.getMessage());
		}
		return resultado;
	}
	public boolean atualizar() {
		boolean resultado = false;
		
		return resultado;
	}
	public boolean deletar() {
		boolean resultado = false;
		
		return resultado;
	}
	
	
}