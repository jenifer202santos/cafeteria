# Cafeteria CoffeeBreak

Aplicação sobre um funcionamento de cafeteria. O sistema tem como principal função o cadastramento de funcionário e a realização de pedidos. Tem relação diretamente com o outro projeto sobre Cafeteria feito em PHP, que se consiste em fazer o pedido online e ele ser mostrado através da tabela do outro projeto.

# Funcionalidades

*  Sistema de Login e Cadastro
*  Registro de Funcionários
*  Realização de pedidos
*  Sessão
*  Possui CRUD
* Tabela de pedidos



